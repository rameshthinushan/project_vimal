
import { Route, Routes,  } from "react-router-dom"
import { ForgetPassword, Login } from './components/Auth/Auth';
// import Home from './components/Home';
import Layout from './layout/Layout'


const RoutesPath = () => {
	return (
		<Routes>
      <Route path="/" element={<Layout/>}>
        <Route path=':navation' element={<Layout/>}/>
      </Route>
      <Route path="/login" element={<Login/>}/>
      <Route path="/forget-password" element={<ForgetPassword/>}/>
			<Route exact path="/404" name="Page 404" element={<h1> Page404 </h1> } />
      <Route exact path="/500" name="Page 500" element={<h1> Page 500 </h1> } />
    </Routes>
	)
}

export default RoutesPath
import {Row, Col, Card , Table } from "react-bootstrap"
import { Link } from "react-router-dom"

const Estimate = () => {
  return (
    <Row className="mt-4">
      <Col>
        <Card className="border-0 card-shadow rounded-4">
          <Card.Body>
            Estimate
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}

export default Estimate
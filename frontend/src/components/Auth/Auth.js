import { Row, Col, Container } from 'react-bootstrap';
import { Link } from "react-router-dom";

const ForgetPassword = () => {
  return (
    <Container 
      fluid 
      className="fixed-container d-grid align-items-center"
    >
      <Row>
        <Col 
          className="bg-white box-container col col-10 col-lg-3 mx-auto rounded-4 p-5"
        >
          <Row> 
            <Col className="mb-5">
              <h2 
                className="mb-4 text-center text-purple"
              >Forget Password</h2>
            </Col>
          </Row>
          <Row> 
            <Col>
              <div 
                className="mb-4"
              >
                <input 
                  type="text" 
                  className="border-0 border-bottom form-control rounded-0 form-control-sm" 
                  placeholder="Email" 
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col>
              <button 
                className="btn-auth bg-white border-0 btn-submit rounded-0 text-purple"
              >
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                Submit
              </button>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  )
}

const Login = () => {
  return (
    <Container 
      fluid 
      className="fixed-container d-grid align-items-center"
    >
      <Row>
        <Col 
          className="bg-white box-container col col-10 col-lg-3 mx-auto rounded-4 p-5"
        >
          <Row> 
            <Col className="mb-5">
              <h2 
                className="mb-4 text-center text-purple"
              >Login</h2>
            </Col>
          </Row>
          <Row> 
            <Col>
              <div 
                className="mb-4"
              >
                <input 
                  type="text" 
                  className="border-0 border-bottom form-control rounded-0 form-control-sm" 
                  placeholder="UserName" 
                />
              </div>
            </Col>
          </Row>
          <Row> 
            <Col>
              <div 
                className="mb-4"
              >
                <input 
                  type="password" 
                  className="border-0 border-bottom form-control rounded-0 form-control-sm" 
                  placeholder="Password"
                />
              </div>
            </Col>
          </Row>
          <Row>
            <Col 
              className="text-end small text-purple"
            >Forget Password</Col>
          </Row>
          <Row>
            <Col>
              <button 
                className="btn-auth bg-white border-0 btn-submit rounded-0 text-purple"
              >
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                Submit
              </button>
            </Col>
          </Row>
          <Row>
            <Col 
              className="text-center mt-5 small"
            >
              <Link 
                to="/forget-password"
                className="text-purple text-decoration-none"
              > 
                Create New Partner 
              </Link>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  )
}

export {ForgetPassword, Login}


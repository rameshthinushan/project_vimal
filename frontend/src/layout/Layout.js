
import Dashboard from './Dashboard';
import { Container } from 'react-bootstrap';

const Layout = () => {
	return(
		<Container fluid>
			<Dashboard/>
		</Container>
	)
}



export default Layout
import { useParams, Link } from 'react-router-dom'
import { Row, Col } from 'react-bootstrap';

const NavItem = () => {
  const list = [{
    text: 'Dashboard',
    to: '/overview',
    icon: 'bi bi-house me-2',
    class: ''            
  }, {
    text: 'Estimate',
    to: '/estimate',
    icon: 'bi bi-bar-chart me-2',
    class: ''            
  }, {
    text: 'Sales',
    to: '/sales',
    icon: 'bi bi-airplane me-2',
    class: ''            
  }, {
    text: 'Purchase',
    to: '/purchase',
    icon: 'bi bi-bag-check me-2',
    class: ''            
  }, {
    text: 'Production',
    to: '/production',
    icon: 'bi bi-bag-check me-2',
    class: ''            
  }, {
    text: 'Settings',
    to: '/settings',
    icon: 'bi bi-gear me-2',
    class: ''            
  }, {
    text: 'User',
    to: '/user',
    icon: 'bi bi-people me-2',
    class: ''            
  }, 
];

  return (
    <section>
      <Row>
        <Col className='mt-3 mb-4'>
          <img 
            src='https://preview.webpixels.io/web/img/logos/clever-primary.svg' 
            alt='...'
          />
        </Col>
      </Row>
      { list.map((item, index) => (
        <Row 
          key={index}
          className='p-3'
        >
          <Col className={`${item.class}`}>
            <i className={item.icon}></i>
            <Link 
              to={item.to} 
              className='text-decoration-none text-dark fw-medium hoverable-text'
            > {item.text} </Link>
          </Col>
        </Row>
      )) }
      <Contact/>
    </section>
  );
}

const Contact = () => {
  return (
    <div>
      <Row className='mb-3 mt-4'>
        <Col className='text-muted text-uppercase text-xs'>Contact</Col>
      </Row>
      <Row className='align-items-center mb-5'>
        <Col className='col-3'>
          <img 
            src="site-image/dog.jpeg" 
            alt="" 
            height="50" 
            className='rounded-circle'
          />
        </Col>
        <Col className='col-6'>
          <div className='fw-semibold'>Thinu</div>
          <div className='text-xs text-secondary'>Paris, FR</div>
        </Col>
        <Col className='col-2'>
          <i class="bi bi-chat"></i>
        </Col>
      </Row>
    </div>
  )
}

export default NavItem


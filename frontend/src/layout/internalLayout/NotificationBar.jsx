import {Row, Col } from "react-bootstrap"

const NotificationBar = () => {
  return (
    <Row>
      <Col className="bg-white">
        <Row className="d-lg-none border-bottom p-3 align-items-center">
          <Col className="col-3 fs-3 ms-3">
            <i className="bi bi-list"></i>
          </Col>
          <Col className="col-4">
            <img 
              src='https://preview.webpixels.io/web/img/logos/clever-primary.svg' 
              alt='...'
              height="30"
            />
          </Col>
          <Col className="col-4 text-end">
            <img 
              src="/site-image/man.png" 
              alt="" 
              height="30"
            />
          </Col>
        </Row>
        <Row className="p-4 align-items-center">
          <Col className="col-lg-10 fs-3 col-6"> Application </Col>
          <Col className="col-lg-2 text-end d-lg-flex d-none col-6">
            <a href="#" className="btn d-inline-flex btn-sm btn-neutral border-base mx-1">
              <span className=" pe-2">
                <i className="bi bi-pencil"></i>
              </span>
              <span>Edit</span>
            </a>
            <a href="#" className="btn d-inline-flex btn-sm btn-primary mx-1">
              <span className=" pe-2">
                <i className="bi bi-plus"></i>
              </span>
              <span>Create</span>
            </a>
          </Col>
          <Col className="d-lg-none col-6 text-end">
            <a href="#" className="btn d-inline-flex btn-sm btn-neutral border-base mx-1">
              <i className="bi bi-pencil"></i>
            </a>
            <a href="#" className="btn d-inline-flex btn-sm btn-primary mx-1">
              <i className="bi bi-plus"></i>
            </a>
          </Col>
        </Row>
        <Row>
          <Col className="col-lg-6 col-8 ms-4 small">
            <Row>
              <Col className="col-lg-2 col-md-3 col-3 pb-3 min-nav-collection">Estimate</Col>
              <Col className="col-lg-2 col-md-3 col-3 pb-3">Sales</Col>
              <Col className="col-lg-2 col-md-3 col-3 pb-3"> Purchase </Col>
              <Col className="col-lg-2 col-md-3 col-3 pb-3"> Production </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

export default NotificationBar
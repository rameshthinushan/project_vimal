import {Row, Col, Card } from "react-bootstrap"

const Cards = () => {
  return (
    <Row className="mt-4">
      <Col className="col-lg-3 col-md-6 col-12 mb-lg-0 mb-3">
        <Card className="border-0 card-shadow rounded-4">
          <Card.Body>
            <Row>
              <Col>
                <span class="h6 font-semibold text-muted text-sm d-block mb-2 fs-6 small">Budget</span>
                <span class="h3 font-bold mb-0">$750.90</span>
              </Col>
              <Col className="col-auto">
                <div class="icon icon-shape bg-tertiary text-white text-lg rounded-circle">
                  <i class="bi bi-credit-card"></i>
                </div>
              </Col>
            </Row>
            <div class="mt-2 mb-0 text-sm">
              <span class="badge badge-pill bg-soft-success text-success me-2">
                <i class="bi bi-arrow-up me-1"></i>13%
              </span>
              <span class="text-nowrap text-xs text-muted">Since last month</span>
            </div>
          </Card.Body>
        </Card>
      </Col>
      <Col className="col-lg-3 col-md-6 col-12 mb-lg-0 mb-3">
        <Card className="border-0 card-shadow rounded-4">
          <Card.Body>
            <Row>
              <Col>
                <span class="h6 font-semibold text-muted text-sm d-block mb-2 fs-6 small">Budget</span>
                <span class="h3 font-bold mb-0">$750.90</span>
              </Col>
              <Col className="col-auto">
                <div class="icon icon-shape bg-primary text-white text-lg rounded-circle">
                  <i class="bi bi-people"></i>
                </div>
              </Col>
            </Row>
            <div class="mt-2 mb-0 text-sm">
              <span class="badge badge-pill bg-soft-success text-success me-2">
                <i class="bi bi-arrow-up me-1"></i>13%
              </span>
              <span class="text-nowrap text-xs text-muted">Since last month</span>
            </div>
          </Card.Body>
        </Card>
      </Col>
      <Col className="col-lg-3 col-md-6 col-12 mb-lg-0 mb-3">
        <Card className="border-0 card-shadow rounded-4">
          <Card.Body>
            <Row>
              <Col>
                <span class="h6 font-semibold text-muted text-sm d-block mb-2 fs-6 small">Budget</span>
                <span class="h3 font-bold mb-0">$750.90</span>
              </Col>
              <Col className="col-auto">
                <div class="icon icon-shape bg-info text-white text-lg rounded-circle">
                  <i class="bi bi-clock-history"></i>
                </div>
              </Col>
            </Row>
            <div class="mt-2 mb-0 text-sm">
              <span class="badge badge-pill bg-soft-danger text-danger me-2">
                <i class="bi bi-arrow-up me-1"></i>13%
              </span>
              <span class="text-nowrap text-xs text-muted">Since last month</span>
            </div>
          </Card.Body>
        </Card>
      </Col>
      <Col className="col-lg-3 col-md-6 col-12 mb-lg-0 mb-3">
        <Card className="border-0 card-shadow rounded-4">
          <Card.Body>
            <Row>
              <Col>
                <span class="h6 font-semibold text-muted text-sm d-block mb-2 fs-6 small">Budget</span>
                <span class="h3 font-bold mb-0">$750.90</span>
              </Col>
              <Col className="col-auto">
                <div class="icon icon-shape bg-warning text-white text-lg rounded-circle">
                  <i class="bi bi-minecart-loaded"></i>
                </div>
              </Col>
            </Row>
            <div class="mt-2 mb-0 text-sm">
              <span class="badge badge-pill bg-soft-success text-success me-2">
                <i class="bi bi-arrow-up me-1"></i>13%
              </span>
              <span class="text-nowrap text-xs text-muted">Since last month</span>
            </div>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  )
}

export default Cards
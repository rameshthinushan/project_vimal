import {Row, Col } from "react-bootstrap"
import { useParams } from "react-router-dom"

import Cards from "./internalLayout/Cards"
import NotificationBar from "./internalLayout/NotificationBar"
import Estimate from "../components/Pages/Estimate"

const Content = () => {
  // let { navation } = useParams()
  // console.log(navation)
  // navation = navation.charAt(0).toUpperCase() + navation.slice(1);

  return (
    <section>
      {/* top of page navition bar start */}
      <Row>
        <Col className="card-shadow">
          <NotificationBar/>
        </Col>
      </Row>
      {/* top of page navition bar end */}
      {/* card section strat */}
      <Row>
        <Col>  
          <Cards/>
        </Col>
      </Row>
      {/* card section end */}
      {/* dynamic contant start*/}
      <Row>
        <Col>
          <Estimate/>
        </Col>
      </Row>
      {/* dynamic contant end*/}
    </section>
  )
}

export default Content
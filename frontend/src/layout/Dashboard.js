import {Row, Col} from 'react-bootstrap'

import NavItem from './internalLayout/NavItem';
import Content from './Content';

export default function Dashboard () {
  return (
    <section>
      <Row>
        <Col>
          <Row>
            {/* side navication bar start*/}
            <Col className='col-lg-2 d-lg-inline d-none bg-white'>
              <NavItem/>
            </Col>
            {/* side navication bar start*/}
            {/* main content start*/}
            <Col className='col-lg-10 bg-surface-secondary border-start'>
              <Content/>
            </Col>
            {/* main content start*/}
          </Row>
        </Col>
      </Row>
    </section>
  );
}


